# BOC-1 - Project Summary of OBDID 

For more information, please contact:  
Caspar Roelofs, caspar@gimly.io
Abe Scholte, ascholte@sphereon.com
Colin Campbell, colin@off-blocks.com

## Summary 

OBDID is a platform for digital agreements and transactions between people and businesses. Re-imagining the way we transact using verifiable credentials improves upon legacy competition in a number of ways, whilst vastly expanding the scope of signatures beyond only documents.    

Our project initially focusses on applying SSI technology to traditional use cases - requesting and tracking signed agreements, data, and forms whilst complying with current eIDAS guidelines. Either remotely, or in-person; bridging the gap between the physical and digital worlds.    

In addition to our business approach, our technological approach seeks to provide standardized SSI components that are usable in every SSI solution. This lays the groundwork for core functionalities such as enabling the resolution of identities, as well as the issuing, exchanging, and verification of credentials. 

## Business Problem 

Digital signatures haven’t changed in twenty years. The increasing need for them has forced us to accept their inherent limitations as standard; namely cost, versatility and legality. This has conditioned us to only transact within a narrow band of pre-defined workflows with every transaction represented as a PDF file  embedded with a costly certificate.    

The move towards remote or contactless work practices, especially across borders, needs a better method of signing transactions. One that’s more secure, flexible, and cost-effective. Using SSI we can finally transact in the digital world, with the same power and freedom that a written signature affords us today. 

## (Technical) Solution 
 
| Solution                    | Description                                                                                       | Contribution                                                      | 
|-----------------------------|---------------------------------------------------------------------------------------------------|-------------------------------------------------------------------| 
| Mobile Holder Wallet        |     Built-in issuance, verification, and presentation functionality; connected to a Factom DID    | Allows the user to be in charge of presenting his/her credentials | 
| Issuer API                  | A W3C Conforming API for organizational credential issuance                                       | Allows anyone to issue VCs                                        | 
| Verifier API                | A W3C Conforming API for organizational credential and presentation verification                  | Allows anyone independently verify any credential or presentation | 
| DID Creation                | Can create DIDs                                                                                   | Allows creation of DIDs                                           | 
| Public DID Resolver         | Can resolve DIDs                                                                                  | Allows resolving of DIDs                                          | 
| Request Credential Webapp   | Requests a credential by providing a QR code                                                      | Allows a quick and secure way to digitally request credentials    | 
| Automatic Credential lookup | Automatically checks what available credentials within the mobile App are required by the request | Allows quick selection of credentials to be shared                | 
| Holder EDV                  | An Encrypted Data Vault that stores credentials                                                   | Allows users to securely share credentials with others            | 

## Integration 

The core components of our solution provide standard, essential SSI functionality. In order to be able to make effective use of SSI functionality, parties need to be able to do things like resolve identities, issue credentials, exchange credentials, and verify them.  

Both our Issuer API and Verifier API are conformant to the standard set by the W3C-CCG, and have already been used in a demonstration of interoperability with DHS-SVIP members. 

Our DID management comes with two parts, DID creation and DID resolution. Our DID creation is wrapped in a simple API that can be used to create new Factom DIDs our DID resolver has a publicly available driver as part of the UniversalResolver. This provides the two primary components around DID management. 

Our Holder Encrypted Data Vault provides a means to exchange credentials in a safe and secure way. It follows an emerging specification (also known as Secure Data Stores), that forms a work item between DIF and W3C-CCG. This is a key part of interop that still needs to be expanded upon, and we plan on contributing to those efforts. 

## Interoperability Demonstration 

Our team, and our tech, have previously been involved in a demonstration of interop as a subcontractor for a party in the DHS-SVIP program. One of the key factors that made that successful was the inter-team consolidation around a set of specifications, as well as joint work on test suites and API definitions. 

We are looking to leverage that experience to help build an interop-focused eSSIF-Labs community. Our tech is already conforming to W3C-CCG specs, however, from what we can see within the various companies making up the eSSIF-Labs business call, these are not the only specs being represented. For this, we see the efforts of the DIF Interop Working Group as essential to enabling a broader demonstration of interoperability as part of eSSIF-Labs. The goal of the DIF Interop Working group is to organize interoperability not just within SSI communities, but also between them (ie. Between Hyperledger, W3C, etc). 